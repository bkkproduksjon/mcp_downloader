#download.py
import requests
import datetime
import sys
import errno
import json
import os
import urllib3
urllib3.disable_warnings()

# load config
configfile = "npconfig.txt"
configs = json.load(open(configfile, 'r'))

# saves file to output directory
def write_output(response_body):
    filename = configs["output_directory"] + configs["filename_part"] + configs["output_format"]
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    open(filename, 'wb').write(response_body)

# gets current date and time as string (for logging)
def datetime_string():
    return datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')

# write 'msg' string to log
def write_log(msg):
    open(configs["log_file_path"], 'a').write(datetime_string() + ": " + msg + '\n')

# gets 'dd-mm-yyyy'-formatted date string 'daysahead' (0, > 0 or < 0) days ahead
def date_string(daysahead = 0):
    return (datetime.datetime.today() + datetime.timedelta(days=daysahead)).strftime(configs["date_format"])

# Builds a URL string
# INPUT: bool xls - whether we try fetching xls or xlsx
#        int daysahead -- 0 if we fetch current date, 1 if we fetch "tomorrow's" price curves, negative integer if previous dates
def url_string(xls, daysahead = 0):
    url = configs["mcp_url"] + configs["filename_part"] + "_" + date_string(daysahead) + configs["append_time_string"] + configs["format_source"]
    if xls:
        write_log("Trying to fetch " + url)
        return url
    else:
        url += 'x'
        write_log("Trying to fetch " + url)
        return url

write_log("---- Started MCP downloader ----")

if configs.get("get_date") == "latest":
    # check if "tomorrow's" curves are uploaded at nordpoolgroup.com
    daysahead = 1
    # check for .xls first
    write_log("Checking for tomorrow's curve data")
    r = requests.get(url_string(True, daysahead), verify = False)
    if r.ok:
        # write to filename without date, to overwrite old file and re-use excel file import query
        write_output(r.content)
    else:
        # check for .xlsx:
        r = requests.get(url_string(False, daysahead), verify = False)
        if r.ok:
            write_output(r.content)
        else:
            # fall-back to current date (omit second arg for url_string())
            write_log("Falling back to current date.")
            r = requests.get(url_string(True), verify = False)
            if r.ok:
                # write to filename without date, to overwrite old file and re-use excel file import query
                write_output(r.content)
            else:
                # check for .xlsx:
                r = requests.get(url_string(False), verify = False)
                if r.ok:
                    write_output(r.content)
                else:
                    write_log("Can't download MCP curve data. Check the URL in config file.")
else:
    # fall-back to current date
    write_log("Getting current date curve data")
    r = requests.get(url_string(True), verify = False)
    if r.ok:
        # write to filename without date, to overwrite old file and re-use excel file import query
        write_output(r.content)
    else:
        # check for .xlsx:
        r = requests.get(url_string(False), verify = False)
        if r.ok:
            write_output(r.content)
        else:
            write_log("Can't download MCP curve data. Check the URL in config file.")
